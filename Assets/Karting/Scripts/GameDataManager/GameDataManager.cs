using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class GameDataManager : MonoBehaviour
{
    [Tooltip("If true load game data at Awake.")] 
    [SerializeField] private bool LoadGameAtAwake = true;

    [Tooltip("If true save the game before Disable.")]
    [SerializeField] private bool SaveGameOnDisable = true;

    /// <summary>
    /// Game data to save/load.
    /// </summary>
    GameData gameData = null;
    public GameData GameData
    {
        get
        {
            if (gameData == null)
            {
                LoadGameData();
            }
            return gameData;
        }
        set
        {
            gameData = value;
            SaveGameData();
        }
    }

    /// <summary>
    /// File system�s directory to save/load game data.
    /// </summary>
    const string DIRECTORY_NAME = "savedGame";

    /// <summary>
    /// Name of file to load/save game data.
    /// </summary>
    const string FILEPATH = "gameData.dat";

    /// <summary>
    /// Path of directory to save/load game data.
    /// </summary>
    string directoryPath;

    /// <summary>
    /// Path of file to save/load game data.
    /// </summary>
    string filePath;

    /// <summary>
    /// Format to conver game data.
    /// </summary>
    BinaryFormatter binaryFormatter = new BinaryFormatter();

    //Data stream
    FileStream fileStream;

    void Awake()
    {
        directoryPath = Path.Combine(Application.persistentDataPath, DIRECTORY_NAME);
        filePath = Path.Combine(directoryPath, FILEPATH);
        if (LoadGameAtAwake)
        {
            LoadGameData();
        }
    }

    void OnDisable()
    {
        if (SaveGameOnDisable)
        {
            SaveGameData();
        }
    }

    /// <summary>
    /// Save game data. Create directory and file if not exist.
    /// </summary>
    public void SaveGameData()
    {
        if (!Directory.Exists(directoryPath))
        {
            Directory.CreateDirectory(directoryPath);
        }
        fileStream = new FileStream(filePath, FileMode.Create);
        binaryFormatter.Serialize(fileStream, gameData);
        fileStream.Close();
    }

    /// <summary>
    /// Load game data. Save default game data if not exist.
    /// </summary>
    void LoadGameData()
    {
        Debug.Log(filePath);
        if (File.Exists(filePath))
        {
            fileStream = new FileStream(filePath, FileMode.Open);
            gameData = binaryFormatter.Deserialize(fileStream) as GameData;
            fileStream.Close();
        }
        else
        {
            gameData = new GameData();
            SaveGameData();
        }
    }
}