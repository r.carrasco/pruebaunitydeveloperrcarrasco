/// <summary>
/// Game data class to save/load game.
/// </summary>
[System.Serializable]
public class GameData
{
    /// <summary>
    /// Best time in all games
    /// </summary>
    public float BestTime { get; set; } = 0f;
    public string BestTimeString { get; set; } = "";

    /// <summary>
    /// Total coins taken in all games
    /// </summary>
    public int TotalCoins { get; set; } = 0;

    /// <summary>
    /// first game saved on device
    /// </summary>
    public GameData()
    {
        BestTime = -1f;
        BestTimeString = "";
        TotalCoins = 0;
    }
}