﻿using UnityEngine;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    [Tooltip("Text for bestTime")]
    [SerializeField] private Text bestTime = null;

    [Tooltip("Text for totalCoins")]
    [SerializeField] private Text totalCoins = null;
    
    /// <summary>
    /// Game data loaded.
    /// </summary>
    private GameData gameData = null;

    [Tooltip("gameDataManager reference for display loaded data")]
    [SerializeField] private GameDataManager gameDataManager = null;

    void Start()
    {
        gameData = gameDataManager.GameData;
        bestTime.text = gameData.BestTimeString;
        totalCoins.text = gameData.TotalCoins.ToString();
    }
}