﻿using UnityEngine;
using UnityEngine.EventSystems;

public class ScreenInputButton : MonoBehaviour,
                                 IPointerDownHandler, IPointerUpHandler
{
    private enum AxisType {Horizontal, Vertical}
    [SerializeField] AxisType axisType = AxisType.Horizontal;
    [SerializeField] float value = 1f;

    [Tooltip("ScreenInput to send input value")]
    [SerializeField] ScreenInput screenInput = null;

    /// <summary>
    /// Actions when tap/release the screen button
    /// </summary>
    /// <param name="ped"></param>
    public void OnPointerDown(PointerEventData ped)
    {
        if(axisType.Equals(AxisType.Horizontal))
        {
            screenInput.HorizontalInput(value);
        }
        else if(axisType.Equals(AxisType.Vertical))
        {
            screenInput.VerticalInput(value);
        }
    }

    public void OnPointerUp(PointerEventData ped)
    {
        if (axisType.Equals(AxisType.Horizontal))
        {
            screenInput.HorizontalInput(0f);
        }
        else if (axisType.Equals(AxisType.Vertical))
        {
            screenInput.VerticalInput(0f);
        }
    }
}