﻿using UnityEngine;

public class ExitButtonController : MonoBehaviour
{
    public void ExitGame()
    {
        Application.Quit();
    }
}