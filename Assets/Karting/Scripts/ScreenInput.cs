﻿using UnityEngine;
using KartGame.KartSystems;

public class ScreenInput : BaseInput
{
    Vector2 input = new Vector2(0f, 0f);

    public void HorizontalInput(float value)
    {
        input.x = value;
    }

    public void VerticalInput(float value)
    {
        input.y = value;
    }

    public override Vector2 GenerateInput()
    {
        return input;
    }
}