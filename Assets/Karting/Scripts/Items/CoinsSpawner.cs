﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsSpawner : MonoBehaviour
{
    [Tooltip("Coin prefab to instantiate.")]
    [SerializeField] GameObject prefabToSpawn = null;

    [Tooltip("Number of instances to generate")]
    [SerializeField] int numberOfInstances = 0;

    [Tooltip("Starts with all coins in scene?")]
    [SerializeField] bool activateAllCoinsAtStart = true;

    /// <summary>
    [Tooltip("Wait time to spawn a coin.")]
    /// </summary>
    [SerializeField] float spawnTime = 0f;

    /// <summary>
    /// Spawn flag.
    /// </summary>
    bool canSpawn = true;

    /// <summary>
    /// Coins and spawn points pools.
    /// </summary>
    List<GameObject> ActiveCoins = new List<GameObject>();
    List<GameObject> InactiveCoins = new List<GameObject>();
    [SerializeField] List<Transform> availableSpawnPoints = new List<Transform>();
    List<Transform> notAvailableSpawnPoints = new List<Transform>();

    [Tooltip("Displays coins taken")]
    [SerializeField] TimeDisplayItem CoinsText = null;
    /// <summary>
    /// Coins taken this scene
    /// </summary>
    public int Coins { get; set; }

    [Tooltip("Save coins on GameData")]
    [SerializeField]  GameDataManager gameDataManager = null;

    /// <summary>
    /// Game data to save on device
    /// </summary>
    GameData gameData;

    void Start()
    {
        //load game data for save coins
        gameData = gameDataManager.GameData;
        if (availableSpawnPoints.Count <= 0)
        {
            Debug.LogError("Cannot spawn without a spawnPoint. Please add spawnPoints to CoinsSpawner");
        }

        for (int i = 0; i < numberOfInstances; i++)
        {
            GameObject instance = Instantiate(prefabToSpawn);
            instance.GetComponent<CoinController>().CoinsSpawner = this;
            instance.SetActive(false);
            InactiveCoins.Add(instance);
        }
        if (activateAllCoinsAtStart)
        {
            for(int j = 0; j < numberOfInstances; j++)
            {
                SpawnCoin();
            }
        }
    }
    void Update()
    {
        if (InactiveCoins.Count > 0 && canSpawn)
        {
            StartCoroutine(SpawnCoinCoroutine());
        }
    }

    IEnumerator SpawnCoinCoroutine()
    {
        canSpawn = false;
        yield return new WaitForSeconds(spawnTime);
        SpawnCoin();
        canSpawn = true;
    }

    void SpawnCoin()
    {
        if (InactiveCoins.Count > 0 && availableSpawnPoints.Count > 0)
        {
            Transform chosenSpawnPoint = GetRandomSpawnPoint();
            DeactivateSpawnPoint(chosenSpawnPoint);
            InactiveCoins[0].GetComponent<CoinController>().SpawnPointLocked = chosenSpawnPoint;
            InactiveCoins[0].transform.SetPositionAndRotation(chosenSpawnPoint.position, chosenSpawnPoint.rotation);
            ActiveCoins.Add(InactiveCoins[0]);
            InactiveCoins[0].SetActive(true);
            InactiveCoins.RemoveAt(0);
        }
    }

    Transform GetRandomSpawnPoint()
    {
        Random.InitState(System.DateTime.Now.Millisecond);
        return availableSpawnPoints[Random.Range(0, availableSpawnPoints.Count)];
    }

    public void DeactivateCoin(GameObject coin)
    {
        gameData.TotalCoins += coin.GetComponent<CoinController>().Value;
        ActiveCoins.Remove(coin);
        InactiveCoins.Add(coin);
        ActivateSpawnPoint(coin.GetComponent<CoinController>().SpawnPointLocked);
        Coins++;
        CoinsText.SetText(Coins.ToString());
    }

    void ActivateSpawnPoint(Transform spawnPoint)
    {
        notAvailableSpawnPoints.Remove(spawnPoint);
        availableSpawnPoints.Add(spawnPoint);
    }

    void DeactivateSpawnPoint(Transform spawnPoint)
    {
        availableSpawnPoints.Remove(spawnPoint);
        notAvailableSpawnPoints.Add(spawnPoint);
    }
}