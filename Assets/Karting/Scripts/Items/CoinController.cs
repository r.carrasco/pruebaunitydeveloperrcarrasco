﻿using UnityEngine;

public class CoinController : MonoBehaviour
{
    /// <summary>
    /// Coin value
    /// </summary>
    public int Value { get; set; } = 1;

    [Tooltip("Layers to trigger with")]
    [SerializeField] LayerMask layerMask = ~0;

    [Header("SoundsSettings")]
    [Tooltip("Sound played when pickup")]
    [SerializeField] AudioClip pickupSound = null;
    [SerializeField] public float volume = 100f;

    /// <summary>
    /// Coins pool.
    /// </summary>
    public CoinsSpawner CoinsSpawner { get; set; }

    /// <summary>
    /// Spawn point to liberate when the coin is taken
    /// </summary>
    public Transform SpawnPointLocked { get; set; }

    void OnTriggerEnter(Collider collider)
    {
        if (layerMask.ContainsGameObjectsLayer(collider.gameObject))
        {
            CoinsSpawner.DeactivateCoin(gameObject);
            AudioSource.PlayClipAtPoint(pickupSound, transform.position, volume);
            gameObject.SetActive(false);
        }
    }
}